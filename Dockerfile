FROM golang:latest

WORKDIR /go/src/app

RUN go get -d -v github.com/ndecker/fritzbox_exporter
RUN go install -v github.com/ndecker/fritzbox_exporter

CMD [ "fritzbox_exporter"]